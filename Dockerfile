FROM node:latest

RUN npm install sails -g

RUN mkdir /www
COPY ./ /www

WORKDIR /www

RUN npm install