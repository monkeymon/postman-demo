/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const uuidv4 = require('uuid/v4');

module.exports = {
  getMe: function(req,res) {
    if(req.params.token) {
      Token.findOne({
        token: req.params.token
      }, function(err, token) {
        if(token) {
          res.json(token.user)
        } else {
          res.notFound("Token not found or invalid")
        }
      })
    } else {
      res.badRequest("Token not provided or not found")
    }
  },
	login: function(req,res) {
    if(req.body.password != 'password123') {
      res.forbidden("incorrect password")
      return
    }
    User.findOne({username: req.body.username})
      .exec(function(err, userModel) {
        if(err) {
          res.json(err)
          return
        }
        if(!userModel) {
          res.forbidden("user not found")
          return
        }
        Token.create({
          token: uuidv4(),
          user:userModel
        }, function(err, token) {
          res.created(token)
        })
      })
  }
};

