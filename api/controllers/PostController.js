/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  list: function(req,res) {
    if(req.params.token) {
      Token.findOne({
        token: req.params.token
      }, function(err, token) {
        if(token) {
          Post.find({
            userID: token.user.id
          }, function(err, post) {
            if(err) {
              res.badRequest(err)
              return
            }
            if(post) {
              res.json(post)
            } else {
              res.badRequest("unknown error")
            }
          })
        } else {
          res.notFound("Token not found or invalid")
        }
      })
    } else {
      res.badRequest("Token not provided")
    }
  },
	create: function(req,res) {
    if(req.params.token && req.body.content) {
      Token.findOne({
        token: req.params.token
      }, function(err, token) {
        if(token) {
          Post.create({
            user: token.user,
            userID: token.userID,
            content: req.body.content
          }, function(err, post) {
            if(err) {
              res.badRequest(err)
              return
            }
            if(post) {
              res.created(post)
            } else {
              res.badRequest("unknown error")
            }
          })
        } else {
          res.notFound("Token not found or invalid")
        }
      })
    } else {
      res.badRequest("Token not provided or content not provided")
    }
  }
};

